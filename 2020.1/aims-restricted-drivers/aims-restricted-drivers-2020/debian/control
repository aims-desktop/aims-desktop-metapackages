Source: aims-restricted-drivers
Section: non-free/kernel
Priority: optional
Maintainer: Jonathan Carter <jonathan@aims.ac.za>
            Jan Groenewald <jan@aims.ac.za>
Build-Depends: debhelper (>= 11)
Standards-Version: 4.2.1 
Homepage: https://desktop.aims.ac.za
Vcs-browser: https://git.aims.ac.za/aims-desktop/aims-desktop-metapackages

Package: aims-restricted-drivers
Architecture: all
Depends: ${misc:Depends}
Recommends: broadcom-sta-dkms,
            firmware-amd-graphics,
            firmware-atherosi,
            firmware-bnx2x,
            firmware-brcm80211,
            firmware-cavium,
            firmware-intel-sound,
            firmware-iwlwifi,
            firmware-misc-nonfree,
            firmware-realtek,
            firmware-atheros
Suggests: vrms
Description: Installs a large variety of drivers and firmware that cannot be included in main
 This is a meta-package that install non-free firmware and drivers from the restricted component.
 .
 From firmware-misc-nonfree:
  * 3Com Typhoon firmware, version 03.001.008 (3com/typhoon.bin)
  * AdvanSys ASC-3550 firmware (advansys/3550.bin)
  * AdvanSys ASC-38C0800 firmware (advansys/38C0800.bin)
  * AdvanSys ASC-38C1600 firmware (advansys/38C1600.bin)
  * AdvanSys SCSI controller microcode (advansys/mcode.bin)
  * Agere/Prism/Symbol Orinoco firmware (AP mode), version 9.48 Hermes I
    (agere_ap_fw.bin)
  * Agere/Prism/Symbol Orinoco firmware (STA mode), version 9.48 Hermes
    I (agere_sta_fw.bin)
  * Abilis Systems AS102 stage 1 firmware (as102_data1_st.hex)
  * Abilis Systems AS102 stage 2 firmware (as102_data2_st.hex)
  * Creative CA0132 DSP firmware (ctefx.bin)
  * Creative CA0132 SpeakerEQ firmware (ctspeq.bin)
  * Chelsio T3 optical EDC firmware (AEL2005 PHY)
    (cxgb3/ael2005_opt_edc.bin)
  * Chelsio T3 twinax EDC firmware (AEL2005 PHY)
    (cxgb3/ael2005_twx_edc.bin)
  * Chelsio T3 twinax EDC firmware (AEL2020 PHY)
    (cxgb3/ael2020_twx_edc.bin)
  * Chelsio T3B protocol engine firmware, version 1.1.0
    (cxgb3/t3b_psram-1.1.0.bin)
  * Chelsio T3C protocol engine firmware, version 1.1.0
    (cxgb3/t3c_psram-1.1.0.bin)
  * Chelsio T3 main firmware, version 7.4.0 (cxgb3/t3fw-7.4.0.bin)
  * Chelsio T3 main firmware, version 7.10.0 (cxgb3/t3fw-7.10.0.bin)
  * Chelsio T3 main firmware, version 7.12.0 (cxgb3/t3fw-7.12.0.bin)
  * Chelsio T4 10GBASE-T firmware (AQ1202 PHY) (cxgb4/aq1202_fw.cld)
  * Chelsio T4 10GBASE-T firmware (BCM84834 PHY) (cxgb4/bcm8483.bin)
  * Chelsio T4 firmware, version 1.15.37.0 (cxgb4/t4fw-1.15.37.0.bin,
    cxgb4/t4fw.bin)
  * Chelsio T5 firmware, version 1.15.37.0 (cxgb4/t5fw-1.15.37.0.bin,
    cxgb4/t5fw.bin)
  * DAB-USB FPGA bitfile (dabusb/bitstream.bin)
  * DAB-USB firmware (dabusb/firmware.fw)
  * Xceive 4000 tuner firmware, version 1.4.1 (dvb-fe-xc4000-1.4.1.fw)
  * Xceive 5000 tuner firmware, version 1.6.114
    (dvb-fe-xc5000-1.6.114.fw)
  * Xceive 5000c tuner firmware, version 4.1.30.7
    (dvb-fe-xc5000c-4.1.30.7.fw)
  * DiBcom dib0700 USB DVB bridge firmware, version 1.20
    (dvb-usb-dib0700-1.20.fw)
  * DiBcom dib9135 DVB-T USB firmware (dvb-usb-it9135-01.fw)
  * DiBcom dib9135 DVB-T USB firmware (dvb-usb-it9135-02.fw)
  * Terratec H5 DRX-K firmware (dvb-usb-terratec-h5-drxk.fw)
  * Intel 82559 D101M microcode (e100/d101m_ucode.bin)
  * Intel 82559 D101S microcode (e100/d101s_ucode.bin)
  * Intel 82551-F and 82551-10 microcode (e100/d102e_ucode.bin)
  * ENE UB6250 MS Pro read/write firmware (ene-ub6250/msp_rdwr.bin)
  * ENE UB6250 MS init firmware (ene-ub6250/ms_init.bin)
  * ENE UB6250 MS read/write firmware (ene-ub6250/ms_rdwr.bin)
  * ENE UB6250 SD init stage 1 firmware (ene-ub6250/sd_init1.bin)
  * ENE UB6250 SD init stage 2 firmware (ene-ub6250/sd_init2.bin)
  * ENE UB6250 SD read/write firmware (ene-ub6250/sd_rdwr.bin)
  * Sensoray 2255 firmware (f2255usb.bin)
  * Micronas GO7007SB encoder stage 1 firmware (go7007/go7007fw.bin)
  * Micronas GO7007SB encoder stage 2 firmware (go7007/go7007tv.bin)
  * WISchip WinFast WalkieTV firmware (go7007/lr192.fw)
  * Plextor ConvertX M402U firmware (go7007/px-m402u.fw)
  * Plextor ConvertX TV402U firmware (go7007/px-tv402u.fw)
  * Sensoray S2250 stage 1 firmware (go7007/s2250-1.fw, s2250_loader.fw)
  * Sensoray S2250 stage 2 firmware (go7007/s2250-2.fw, s2250.fw)
  * WISchip WinFast WalkieTV firmware (go7007/wis-startrek.fw)
  * Intel "Broxton" DMC firmware, version 1.07
    (i915/bxt_dmc_ver1_07.bin, i915/bxt_dmc_ver1.bin)
  * Intel "Skylake" DMC firmware, version 1.23
    (i915/skl_dmc_ver1_23.bin)
  * Intel "Skylake" DMC firmware, version 1.26
    (i915/skl_dmc_ver1_26.bin, i915/skl_dmc_ver1.bin)
  * Intel "Skylake" GuC firmware, version 1.1059 (i915/skl_guc_ver1.bin)
  * Intel "Skylake" GuC firmware, version 4.3 (i915/skl_guc_ver4.bin)
  * kaweth/new_code.bin
  * kaweth/new_code_fix.bin
  * kaweth/trigger_code.bin
  * kaweth/trigger_code_fix.bin
  * Matrox G200 WARP engine microcode (matrox/g200_warp.fw)
  * Matrox G400/G550 WARP engine microcode (matrox/g400_warp.fw)
  * MOXA UPort 1110 firmware (moxa/moxa-1110.fw)
  * MOXA UPort 1130 firmware (moxa/moxa-1130.fw)
  * MOXA UPort 1130I firmware (moxa/moxa-1131.fw)
  * MOXA UPort 1150 firmware (moxa/moxa-1150.fw)
  * MOXA UPort 1150I firmware (moxa/moxa-1151.fw)
  * MOXA UPort 1250 firmware (moxa/moxa-1250.fw)
  * MOXA UPort 1250I firmware (moxa/moxa-1251.fw)
  * MOXA UPort 1410 firmware (moxa/moxa-1410.fw)
  * MOXA UPort 1450 firmware (moxa/moxa-1450.fw)
  * MOXA UPort 1450I firmware (moxa/moxa-1451.fw)
  * MOXA UPort 1610-16 firmware (moxa/moxa-1613.fw)
  * MOXA UPort 1610-8 firmware (moxa/moxa-1618.fw)
  * MOXA UPort 1650-16 firmware (moxa/moxa-1653.fw)
  * MOXA UPort 1650-8 firmware (moxa/moxa-1658.fw)
  * MediaTek MT7601U firmware, version 34 (mt7601u.bin)
  * MediaTek MT7630/MT7650 Bluetooth firmware (mt7650.bin)
  * Multi-Tech USB CDMA modem firmware (mts_cdma.fw)
  * Multi-Tech USB EDGE modem firmware (mts_edge.fw)
  * Multi-Tech USB GPRS modem firmware (mts_gsm.fw)
  * Nvidia GK20A FECS firmware data (nvidia/gk20a/fecs_data.bin)
  * Nvidia GK20A FECS firmware instructions (nvidia/gk20a/fecs_inst.bin)
  * Nvidia GK20A GPCCS firmware data (nvidia/gk20a/gpccs_data.bin)
  * Nvidia GK20A GPCCS firmware instructions
    (nvidia/gk20a/gpccs_inst.bin)
  * Nvidia GK20A bundle init data (nvidia/gk20a/sw_bundle_init.bin)
  * Nvidia GK20A ctx init data (nvidia/gk20a/sw_ctx.bin)
  * Nvidia GK20A method init data (nvidia/gk20a/sw_method_init.bin)
  * Nvidia GK20A non-ctx init data (nvidia/gk20a/sw_nonctx.bin)
  * Nvidia Tegra 124 XHCI firmware, version 45.46
    (nvidia/tegra124/xusb.bin)
  * Nvidia Tegra 210 XHCI firmware, version 50.10
    (nvidia/tegra210/xusb.bin)
  * Intel QAT DH895xCC UOF firmware (qat_895xcc.bin)
  * Intel QAT DH895xCC MMP firmware (qat_895xcc_mmp.bin, qat_mmp.bin)
  * Intel QAT C3xxx UOF firmware (qat_c3xxx.bin)
  * Intel QAT C3xxx MMP firmware (qat_c3xxx_mmp.bin)
  * Intel QAT C62x UOF firmware (qat_c62x.bin)
  * Intel QAT C62x MMP firmware (qat_c62x_mmp.bin)
  * Comtrol RocketPort 2 firmware (rp2.fw)
  * Ralink RT2561 (RT2501/RT5201 chipset) firmware, version 1.2
    (rt2561.bin)
  * Ralink RT2561S (RT2501 Turbo/RT5201 Turbo chipset) firmware, version
    1.2 (rt2561s.bin)
  * Ralink RT2661 (RT2600/RT5600 chipset) firmware, version 1.2
    (rt2661.bin)
  * Ralink RT2760/RT2790/RT2860/RT2890/RT3090
    (RT2700P[D]/RT2700E[D]/RT2800P[D]/RT2800E[D]/RT3000E[D] chipset)
    firmware, version 0.40 (rt2860.bin, rt3090.bin)
  * Ralink RT2870/RT3070/RT3071/RT3072 (RT2800U[D] chipset) firmware,
    version 0.36 (rt2870.bin, rt3070.bin)
  * Ralink RT3071/RT3072 firmware, version 29 (rt3071.bin)
  * Ralink RT3290 firmware, version 1 (rt3290.bin)
  * Ralink RT2571W/RT2671 (RT2501USB/RT5201USB chipset) firmware,
    version 1.8 (rt73.bin)
  * Tehuti network card firmware (tehuti/bdx.bin)
  * Broadcom BCM5703/BCM5704 TSO firmware (tigon/tg3_tso.bin)
  * Broadcom BCM5701A0 firmware (tigon/tg3.bin)
  * Broadcom BCM5705 TSO firmware (tigon/tg3_tso5.bin)
  * Broadcom BCM57766 firmware (tigon/tg357766.bin)
  * ADI Eagle IV ADSL configuration, generic (ueagle-atm/CMV4p.bin.v2)
  * ADI Eagle IV ADSL DSP firmware (ueagle-atm/DSP4p.bin)
  * ADI Eagle IV firmware (ueagle-atm/eagleIV.fw)
  * Conexant CX231xx core firmware (v4l-cx231xx-avcore-01.fw)
  * Conexant CX23418 APU firmware (v4l-cx23418-apu.fw)
  * Conexant CX23418 CPU firmware (v4l-cx23418-cpu.fw)
  * Conexant CX23418 ADEC firmware (v4l-cx23418-dig.fw)
  * Conexant CX25840 firmware (v4l-cx25840.fw)
  * Conexant CX23885 core firmware (v4l-cx23885-avcore-01.fw)
  * VIA VT6656 firmware (vntwusb.fw)
 From firmware-amd-graphics:
  * amdgpu/carrizo_ce.bin
  * amdgpu/carrizo_me.bin
  * amdgpu/carrizo_mec.bin
  * amdgpu/carrizo_mec2.bin
  * amdgpu/carrizo_pfp.bin
  * amdgpu/carrizo_rlc.bin
  * amdgpu/carrizo_sdma.bin
  * amdgpu/carrizo_sdma1.bin
  * amdgpu/carrizo_uvd.bin
  * amdgpu/carrizo_vce.bin
  * amdgpu/fiji_ce.bin
  * amdgpu/fiji_mc.bin
  * amdgpu/fiji_me.bin
  * amdgpu/fiji_mec.bin
  * amdgpu/fiji_mec2.bin
  * amdgpu/fiji_pfp.bin
  * amdgpu/fiji_rlc.bin
  * amdgpu/fiji_sdma.bin
  * amdgpu/fiji_sdma1.bin
  * amdgpu/fiji_smc.bin
  * amdgpu/fiji_uvd.bin
  * amdgpu/fiji_vce.bin
  * amdgpu/polaris10_ce.bin
  * amdgpu/polaris10_mc.bin
  * amdgpu/polaris10_me.bin
  * amdgpu/polaris10_mec.bin
  * amdgpu/polaris10_mec2.bin
  * amdgpu/polaris10_pfp.bin
  * amdgpu/polaris10_rlc.bin
  * amdgpu/polaris10_sdma.bin
  * amdgpu/polaris10_sdma1.bin
  * amdgpu/polaris10_smc.bin
  * amdgpu/polaris10_smc_sk.bin
  * amdgpu/polaris10_uvd.bin
  * amdgpu/polaris10_vce.bin
  * amdgpu/polaris11_ce.bin
  * amdgpu/polaris11_mc.bin
  * amdgpu/polaris11_me.bin
  * amdgpu/polaris11_mec.bin
  * amdgpu/polaris11_mec2.bin
  * amdgpu/polaris11_pfp.bin
  * amdgpu/polaris11_rlc.bin
  * amdgpu/polaris11_sdma.bin
  * amdgpu/polaris11_sdma1.bin
  * amdgpu/polaris11_smc.bin
  * amdgpu/polaris11_smc_sk.bin
  * amdgpu/polaris11_uvd.bin
  * amdgpu/polaris11_vce.bin
  * amdgpu/stoney_ce.bin
  * amdgpu/stoney_me.bin
  * amdgpu/stoney_mec.bin
  * amdgpu/stoney_pfp.bin
  * amdgpu/stoney_rlc.bin
  * amdgpu/stoney_sdma.bin
  * amdgpu/stoney_vce.bin
  * amdgpu/stoney_uvd.bin
  * amdgpu/tonga_ce.bin
  * amdgpu/tonga_mc.bin
  * amdgpu/tonga_me.bin
  * amdgpu/tonga_mec.bin
  * amdgpu/tonga_mec2.bin
  * amdgpu/tonga_pfp.bin
  * amdgpu/tonga_rlc.bin
  * amdgpu/tonga_sdma.bin
  * amdgpu/tonga_sdma1.bin
  * amdgpu/tonga_smc.bin
  * amdgpu/tonga_uvd.bin
  * amdgpu/tonga_vce.bin
  * amdgpu/topaz_ce.bin
  * amdgpu/topaz_mc.bin
  * amdgpu/topaz_me.bin
  * amdgpu/topaz_mec.bin
  * amdgpu/topaz_mec2.bin
  * amdgpu/topaz_pfp.bin
  * amdgpu/topaz_rlc.bin
  * amdgpu/topaz_sdma.bin
  * amdgpu/topaz_sdma1.bin
  * amdgpu/topaz_smc.bin
  * Rage 128 CCE microcode (r128/r128_cce.bin)
  * Radeon HD IGP 7500/7600 series ME microcode (radeon/ARUBA_me.bin)
  * Radeon HD IGP 7500/7600 series PFP microcode (radeon/ARUBA_pfp.bin)
  * Radeon HD IGP 7500/7600 series RLC microcode (radeon/ARUBA_rlc.bin)
  * Radeon HD 6800 series MC microcode (radeon/BARTS_mc.bin)
  * Radeon HD 6800 series ME microcode (radeon/BARTS_me.bin)
  * Radeon HD 6800 series PFP microcode (radeon/BARTS_pfp.bin)
  * Radeon HD 6800 series SMC microcode (radeon/BARTS_smc.bin)
  * Radeon HD 7790/8770/8950 CE microcode (radeon/bonaire_ce.bin)
  * Radeon HD 7790/8770/8950 CE microcode (radeon/BONAIRE_ce.bin)
  * Radeon HD 7790/8770/8950 MC microcode (radeon/bonaire_mc.bin)
  * Radeon HD 7790/8770/8950 MC microcode, version 1
    (radeon/BONAIRE_mc.bin)
  * Radeon HD 7790/8770/8950 MC microcode, version 2
    (radeon/BONAIRE_mc2.bin)
  * Radeon HD 7790/8770/8950 ME microcode (radeon/bonaire_me.bin)
  * Radeon HD 7790/8770/8950 ME microcode (radeon/BONAIRE_me.bin)
  * Radeon HD 7790/8770/8950 MEC microcode (radeon/bonaire_mec.bin)
  * Radeon HD 7790/8770/8950 MEC microcode (radeon/BONAIRE_mec.bin)
  * Radeon HD 7790/8770/8950 PFP microcode (radeon/bonaire_pfp.bin)
  * Radeon HD 7790/8770/8950 PFP microcode (radeon/BONAIRE_pfp.bin)
  * Radeon HD 7790/8770/8950 RLC microcode (radeon/bonaire_rlc.bin)
  * Radeon HD 7790/8770/8950 RLC microcode (radeon/BONAIRE_rlc.bin)
  * Radeon HD 7790/8770/8950 SDMA microcode (radeon/bonaire_sdma.bin)
  * Radeon HD 7790/8770/8950 SDMA microcode (radeon/BONAIRE_sdma.bin)
  * Radeon HD 7790/8770/8950 SDMA1 microcode (radeon/bonaire_sdma1.bin)
  * Radeon HD 7790/8770/8950 series SMC microcode
    (radeon/bonaire_smc.bin)
  * Radeon HD 7790/8770/8950 series SMC microcode
    (radeon/BONAIRE_smc.bin)
  * Radeon HD 7790/8770/8950, 8100/8200/8300/8400 series, and
    "Mullins"/"Beema" IGP UVD microcode (radeon/BONAIRE_uvd.bin)
  * Radeon HD 7790/8770/8950 UVD microcode (radeon/bonaire_uvd.bin)
  * Radeon HD VCE microcode (radeon/BONAIRE_vce.bin)
  * Radeon HD 7790/8770/8950 VCE microcode (radeon/bonaire_vce.bin)
  * Radeon HD 6300/6500/6800 series RLC microcode (radeon/BTC_rlc.bin)
  * Radeon HD 6300 series MC microcode (radeon/CAICOS_mc.bin)
  * Radeon HD 6300 series ME microcode (radeon/CAICOS_me.bin)
  * Radeon HD 6300 series PFP microcode (radeon/CAICOS_pfp.bin)
  * Radeon HD 6300 series SMC microcode (radeon/CAICOS_smc.bin)
  * Radeon HD 6900 series MC microcode (radeon/CAYMAN_mc.bin)
  * Radeon HD 6900 series ME microcode (radeon/CAYMAN_me.bin)
  * Radeon HD 6900 series PFP microcode (radeon/CAYMAN_pfp.bin)
  * Radeon HD 6900 series RLC microcode (radeon/CAYMAN_rlc.bin)
  * Radeon HD 6900 series SMC microcode (radeon/CAYMAN_smc.bin)
  * Radeon HD 5400 series ME microcode (radeon/CEDAR_me.bin)
  * Radeon HD 5400 series PFP microcode (radeon/CEDAR_pfp.bin)
  * Radeon HD 5400 series RLC microcode (radeon/CEDAR_rlc.bin)
  * Radeon HD 5400 series SMC microcode (radeon/CEDAR_smc.bin)
  * Radeon HD 5800/5900 series ME microcode (radeon/CYPRESS_me.bin)
  * Radeon HD 5800/5900 series PFP microcode (radeon/CYPRESS_pfp.bin)
  * Radeon HD 5800/5900 series RLC microcode (radeon/CYPRESS_rlc.bin)
  * Radeon HD 5800/5900 series SMC microcode (radeon/CYPRESS_smc.bin)
  * Radeon HD 5x00 series UVD microcode (radeon/CYPRESS_uvd.bin)
  * Radeon HD 8500M series and R5 M200 CE microcode
    (radeon/hainan_ce.bin)
  * Radeon HD 8500M series and R5 M200 CE microcode
    (radeon/HAINAN_ce.bin)
  * Radeon HD 8500M series and R5 M200 MC microcode
    (radeon/hainan_mc.bin)
  * Radeon HD 8500M series and R5 M200 MC microcode, version 1
    (radeon/HAINAN_mc.bin)
  * Radeon HD 8500M series and R5 M200 MC microcode, version 2
    (radeon/HAINAN_mc2.bin)
  * Radeon HD 8500M series and R5 M200 ME microcode
    (radeon/hainan_me.bin)
  * Radeon HD 8500M series and R5 M200 ME microcode
    (radeon/HAINAN_me.bin)
  * Radeon HD 8500M series and R5 M200 PFP microcode
    (radeon/hainan_pfp.bin)
  * Radeon HD 8500M series and R5 M200 PFP microcode
    (radeon/HAINAN_pfp.bin)
  * Radeon HD 8500M series and R5 M200 RLC microcode
    (radeon/hainan_rlc.bin)
  * Radeon HD 8500M series and R5 M200 RLC microcode
    (radeon/HAINAN_rlc.bin)
  * Radeon HD 8500M series and R5 M200 SMC microcode
    (radeon/hainan_smc.bin)
  * Radeon HD 8500M series and R5 M200 SMC microcode
    (radeon/HAINAN_smc.bin)
  * Radeon R9 290 series CE microcode (radeon/hawaii_ce.bin)
  * Radeon R9 290 series CE microcode (radeon/HAWAII_ce.bin)
  * Radeon R9 290 series MC microcode (radeon/hawaii_mc.bin)
  * Radeon R9 290 series MC microcode, version 1 (radeon/HAWAII_mc.bin)
  * Radeon R9 290 series MC microcode, version 2 (radeon/HAWAII_mc2.bin)
  * Radeon R9 290 series ME microcode (radeon/hawaii_me.bin)
  * Radeon R9 290 series ME microcode (radeon/HAWAII_me.bin)
  * Radeon R9 290 series MEC microcode (radeon/hawaii_mec.bin)
  * Radeon R9 290 series MEC microcode (radeon/HAWAII_mec.bin)
  * Radeon R9 290 series PFP microcode (radeon/hawaii_pfp.bin)
  * Radeon R9 290 series PFP microcode (radeon/HAWAII_pfp.bin)
  * Radeon R9 290 series RLC microcode (radeon/hawaii_rlc.bin)
  * Radeon R9 290 series RLC microcode (radeon/HAWAII_rlc.bin)
  * Radeon R9 290 series SDMA microcode (radeon/hawaii_sdma.bin)
  * Radeon R9 290 series SDMA microcode (radeon/HAWAII_sdma.bin)
  * Radeon R9 290 series SDMA microcode (radeon/hawaii_sdma1.bin)
  * Radeon R9 290 series SMC microcode (radeon/HAWAII_smc.bin)
  * Radeon R9 290 series SMC microcode (radeon/hawaii_smc.bin)
  * Radeon R9 290 series UVD microcode (radeon/hawaii_uvd.bin)
  * Radeon R9 290 series VCE microcode (radeon/hawaii_vce.bin)
  * Radeon HD 5700 series ME microcode (radeon/JUNIPER_me.bin)
  * Radeon HD 5700 series PFP microcode (radeon/JUNIPER_pfp.bin)
  * Radeon HD 5700 series RLC microcode (radeon/JUNIPER_rlc.bin)
  * Radeon HD 5700 series SMC microcode (radeon/JUNIPER_smc.bin)
  * Radeon HD 8100/8200/8300/8400 series CE microcode
    (radeon/kabini_ce.bin)
  * Radeon HD 8100/8200/8300/8400 series CE microcode
    (radeon/KABINI_ce.bin)
  * Radeon HD 8100/8200/8300/8400 series ME microcode
    (radeon/kabini_me.bin)
  * Radeon HD 8100/8200/8300/8400 series ME microcode
    (radeon/KABINI_me.bin)
  * Radeon HD 8100/8200/8300/8400 series MEC microcode
    (radeon/kabini_mec.bin)
  * Radeon HD 8100/8200/8300/8400 series MEC microcode
    (radeon/KABINI_mec.bin)
  * Radeon HD 8100/8200/8300/8400 series PFP microcode
    (radeon/kabini_pfp.bin)
  * Radeon HD 8100/8200/8300/8400 series PFP microcode
    (radeon/KABINI_pfp.bin)
  * Radeon HD 8100/8200/8300/8400 series RLC microcode
    (radeon/kabini_rlc.bin)
  * Radeon HD 8100/8200/8300/8400 series RLC microcode
    (radeon/KABINI_rlc.bin)
  * Radeon HD 8100/8200/8300/8400 series SDMA microcode
    (radeon/kabini_sdma.bin)
  * Radeon HD 8100/8200/8300/8400 series SDMA microcode
    (radeon/KABINI_sdma.bin)
  * Radeon HD 8100/8200/8300/8400 series SDMA1 microcode
    (radeon/kabini_sdma1.bin)
  * Radeon HD 8100/8200/8300/8400 series UVD microcode
    (radeon/kabini_uvd.bin)
  * Radeon HD 8100/8200/8300/8400 series VCE microcode
    (radeon/kabini_vce.bin)
  * Radeon R5/R7 IGP 200 series CE microcode (radeon/kaveri_ce.bin)
  * Radeon R5/R7 IGP 200 series CE microcode (radeon/KAVERI_ce.bin)
  * Radeon R5/R7 IGP 200 series ME microcode (radeon/kaveri_me.bin)
  * Radeon R5/R7 IGP 200 series ME microcode (radeon/KAVERI_me.bin)
  * Radeon R5/R7 IGP 200 series MEC microcode, version 396
    (radeon/kaveri_mec.bin)
  * Radeon R5/R7 IGP 200 series MEC microcode (radeon/KAVERI_mec.bin)
  * Radeon R5/R7 IGP 200 series MEC microcode, version 396
    (radeon/kaveri_mec2.bin)
  * Radeon R5/R7 IGP 200 series PFP microcode (radeon/kaveri_pfp.bin)
  * Radeon R5/R7 IGP 200 series PFP microcode (radeon/KAVERI_pfp.bin)
  * Radeon R5/R7 IGP 200 series RLC microcode (radeon/kaveri_rlc.bin)
  * Radeon R5/R7 IGP 200 series RLC microcode (radeon/KAVERI_rlc.bin)
  * Radeon R5/R7 IGP 200 series SDMA microcode (radeon/kaveri_sdma.bin)
  * Radeon R5/R7 IGP 200 series SDMA microcode (radeon/KAVERI_sdma.bin)
  * Radeon R5/R7 IGP 200 series SDMA1 microcode
    (radeon/kaveri_sdma1.bin)
  * Radeon R5/R7 IGP 200 series UVD microcode (radeon/kaveri_uvd.bin)
  * Radeon R5/R7 IGP 200 series VCE microcode (radeon/kaveri_vce.bin)
  * Radeon "Mullins"/"Beema" IGP CE microcode (radeon/mullins_ce.bin)
  * Radeon "Mullins"/"Beema" IGP CE microcode (radeon/MULLINS_ce.bin)
  * Radeon "Mullins"/"Beema" IGP ME microcode (radeon/mullins_me.bin)
  * Radeon "Mullins"/"Beema" IGP ME microcode (radeon/MULLINS_me.bin)
  * Radeon "Mullins"/"Beema" IGP MEC microcode (radeon/mullins_mec.bin)
  * Radeon "Mullins"/"Beema" IGP MEC microcode (radeon/MULLINS_mec.bin)
  * Radeon "Mullins"/"Beema" IGP PFP microcode (radeon/mullins_pfp.bin)
  * Radeon "Mullins"/"Beema" IGP PFP microcode (radeon/MULLINS_pfp.bin)
  * Radeon "Mullins"/"Beema" IGP RLC microcode (radeon/mullins_rlc.bin)
  * Radeon "Mullins"/"Beema" IGP RLC microcode (radeon/MULLINS_rlc.bin)
  * Radeon "Mullins"/"Beema" IGP SDMA microcode
    (radeon/mullins_sdma.bin)
  * Radeon "Mullins"/"Beema" IGP SDMA microcode
    (radeon/MULLINS_sdma.bin)
  * Radeon "Mullins"/"Beema" IGP SDMA1 microcode
    (radeon/mullins_sdma1.bin)
  * Radeon "Mullins"/"Beema" IGP UVD microcode (radeon/mullins_uvd.bin)
  * Radeon "Mullins"/"Beema" IGP VCE microcode (radeon/mullins_vce.bin)
  * Radeon HD 8500/8600/8700 series CE microcode (radeon/oland_ce.bin)
  * Radeon HD 8500/8600/8700 series CE microcode (radeon/OLAND_ce.bin)
  * Radeon HD 8500/8600/8700 series MC microcode (radeon/oland_mc.bin)
  * Radeon HD 8500/8600/8700 series MC microcode, version 1
    (radeon/OLAND_mc.bin)
  * Radeon HD 8500/8600/8700 series MC microcode, version 2
    (radeon/OLAND_mc2.bin)
  * Radeon HD 8500/8600/8700 series ME microcode (radeon/oland_me.bin)
  * Radeon HD 8500/8600/8700 series ME microcode (radeon/OLAND_me.bin)
  * Radeon HD 8500/8600/8700 series PFP microcode (radeon/oland_pfp.bin)
  * Radeon HD 8500/8600/8700 series PFP microcode (radeon/OLAND_pfp.bin)
  * Radeon HD 8500/8600/8700 series RLC microcode (radeon/oland_rlc.bin)
  * Radeon HD 8500/8600/8700 series RLC microcode (radeon/OLAND_rlc.bin)
  * Radeon HD 8500/8600/8700 series SMC microcode (radeon/oland_smc.bin)
  * Radeon HD 8500/8600/8700 series SMC microcode (radeon/OLAND_smc.bin)
  * Radeon HD IGP 6200/6300/7300 series ME microcode
    (radeon/PALM_me.bin)
  * Radeon HD IGP 6200/6300/7300 series PFP microcode
    (radeon/PALM_pfp.bin)
  * Radeon HD 7800 series CE microcode (radeon/pitcairn_ce.bin)
  * Radeon HD 7800 series CE microcode (radeon/PITCAIRN_ce.bin)
  * Radeon HD 7800 series MC microcode (radeon/pitcairn_mc.bin)
  * Radeon HD 7800 series MC microcode, version 1
    (radeon/PITCAIRN_mc.bin)
  * Radeon HD 7800 series MC microcode, version 2
    (radeon/PITCAIRN_mc2.bin)
  * Radeon HD 7800 series ME microcode (radeon/pitcairn_me.bin)
  * Radeon HD 7800 series ME microcode (radeon/PITCAIRN_me.bin)
  * Radeon HD 7800 series PFP microcode (radeon/pitcairn_pfp.bin)
  * Radeon HD 7800 series PFP microcode (radeon/PITCAIRN_pfp.bin)
  * Radeon HD 7800 series RLC microcode (radeon/pitcairn_rlc.bin)
  * Radeon HD 7800 series RLC microcode (radeon/PITCAIRN_rlc.bin)
  * Radeon HD 7800 series SMC microcode (radeon/pitcairn_smc.bin)
  * Radeon HD 7800 series SMC microcode (radeon/PITCAIRN_smc.bin)
  * Radeon R100-family CP microcode (radeon/R100_cp.bin)
  * Radeon R200-family CP microcode (radeon/R200_cp.bin)
  * Radeon R300-family CP microcode (radeon/R300_cp.bin)
  * Radeon R400-family CP microcode (radeon/R420_cp.bin)
  * Radeon R500-family CP microcode (radeon/R520_cp.bin)
  * Radeon R600 ME microcode (radeon/R600_me.bin)
  * Radeon R600 PFP microcode (radeon/R600_pfp.bin)
  * Radeon R600-family RLC microcode (radeon/R600_rlc.bin)
  * Radeon R600 UVD microcode (radeon/R600_uvd.bin)
  * Radeon R700-family RLC microcode (radeon/R700_rlc.bin)
  * Radeon HD 5500/5600 series ME microcode (radeon/REDWOOD_me.bin)
  * Radeon HD 5500/5600 series PFP microcode (radeon/REDWOOD_pfp.bin)
  * Radeon HD 5500/5600 series RLC microcode (radeon/REDWOOD_rlc.bin)
  * Radeon HD 5500/5600 series SMC microcode (radeon/REDWOOD_smc.bin)
  * Radeon RS600 CP microcode (radeon/RS600_cp.bin)
  * Radeon RS690 CP microcode (radeon/RS690_cp.bin)
  * Radeon RS780 ME microcode (radeon/RS780_me.bin)
  * Radeon RS780 PFP microcode (radeon/RS780_pfp.bin)
  * Radeon RS780 UVD microcode (radeon/RS780_uvd.bin)
  * Radeon RV610 ME microcode (radeon/RV610_me.bin)
  * Radeon RV610 PFP microcode (radeon/RV610_pfp.bin)
  * Radeon RV620 ME microcode (radeon/RV620_me.bin)
  * Radeon RV620 PFP microcode (radeon/RV620_pfp.bin)
  * Radeon RV630 ME microcode (radeon/RV630_me.bin)
  * Radeon RV630 PFP microcode (radeon/RV630_pfp.bin)
  * Radeon RV635 ME microcode (radeon/RV635_me.bin)
  * Radeon RV635 PFP microcode (radeon/RV635_pfp.bin)
  * Radeon RV670 ME microcode (radeon/RV670_me.bin)
  * Radeon RV670 PFP microcode (radeon/RV670_pfp.bin)
  * Radeon RV710 ME microcode (radeon/RV710_me.bin)
  * Radeon RV710 PFP microcode (radeon/RV710_pfp.bin)
  * Radeon RV710 SMC microcode (radeon/RV710_smc.bin)
  * Radeon RV710/RV730/RV740 UVD microcode (radeon/RV710_uvd.bin)
  * Radeon RV730/RV740 ME microcode (radeon/RV730_me.bin)
  * Radeon RV730/RV740 PFP microcode (radeon/RV730_pfp.bin)
  * Radeon RV730 SMC microcode (radeon/RV730_smc.bin)
  * Radeon RV740 SMC microcode (radeon/RV740_smc.bin)
  * Radeon RV770 ME microcode (radeon/RV770_me.bin)
  * Radeon RV770 PFP microcode (radeon/RV770_pfp.bin)
  * Radeon RV770 SMC microcode (radeon/RV770_smc.bin)
  * Radeon RV770 UVD microcode (radeon/RV770_uvd.bin)
  * Radeon HD IGP 6400/6500/6600 series ME microcode
    (radeon/SUMO_me.bin)
  * Radeon HD IGP 6400/6500/6600 series PFP microcode
    (radeon/SUMO_pfp.bin)
  * Radeon HD IGP 6200/6300/6400/6500/6600/7300 series RLC microcode
    (radeon/SUMO_rlc.bin)
  * Radeon HD 6x00/7500 series and IGP 6x00/7300 series UVD microcode
    (radeon/SUMO_uvd.bin)
  * Radeon HD 6370D/6380G/6410D ME microcode (radeon/SUMO2_me.bin)
  * Radeon HD 6370D/6380G/6410D PFP microcode (radeon/SUMO2_pfp.bin)
  * Radeon HD 7900 series CE microcode (radeon/tahiti_ce.bin)
  * Radeon HD 7900 series CE microcode (radeon/TAHITI_ce.bin)
  * Radeon HD 7900 series MC microcode (radeon/tahiti_mc.bin)
  * Radeon HD 7900 series MC microcode, version 1 (radeon/TAHITI_mc.bin)
  * Radeon HD 7900 series MC microcode, version 2
    (radeon/TAHITI_mc2.bin)
  * Radeon HD 7900 series ME microcode (radeon/tahiti_me.bin)
  * Radeon HD 7900 series ME microcode (radeon/TAHITI_me.bin)
  * Radeon HD 7900 series PFP microcode (radeon/tahiti_pfp.bin)
  * Radeon HD 7900 series PFP microcode (radeon/TAHITI_pfp.bin)
  * Radeon HD 7900 series RLC microcode (radeon/tahiti_rlc.bin)
  * Radeon HD 7900 series RLC microcode (radeon/TAHITI_rlc.bin)
  * Radeon HD 7900 series SMC microcode (radeon/tahiti_smc.bin)
  * Radeon HD 7900 series SMC microcode (radeon/TAHITI_smc.bin)
  * Radeon HD 7900 series UVD microcode (radeon/TAHITI_uvd.bin)
  * Radeon HD 7900 series VCE microcode (radeon/TAHITI_vce.bin)
  * Radeon HD 6500/6600/7500 series MC microcode (radeon/TURKS_mc.bin)
  * Radeon HD 6500/6600/7500 series ME microcode (radeon/TURKS_me.bin)
  * Radeon HD 6500/6600/7500 series PFP microcode (radeon/TURKS_pfp.bin)
  * Radeon HD 6500/6600/7500 series SMC microcode (radeon/TURKS_smc.bin)
  * Radeon HD 7700 series CE microcode (radeon/verde_ce.bin)
  * Radeon HD 7700 series CE microcode (radeon/VERDE_ce.bin)
  * Radeon HD 7700 series MC microcode (radeon/verde_mc.bin)
  * Radeon HD 7700 series MC microcode, version 1 (radeon/VERDE_mc.bin)
  * Radeon HD 7700 series MC microcode, version 2 (radeon/VERDE_mc2.bin)
  * Radeon HD 7700 series ME microcode (radeon/verde_me.bin)
  * Radeon HD 7700 series ME microcode (radeon/VERDE_me.bin)
  * Radeon HD 7700 series PFP microcode (radeon/verde_pfp.bin)
  * Radeon HD 7700 series PFP microcode (radeon/VERDE_pfp.bin)
  * Radeon HD 7700 series RLC microcode (radeon/verde_rlc.bin)
  * Radeon HD 7700 series RLC microcode (radeon/VERDE_rlc.bin)
  * Radeon HD 7700 series SMC microcode (radeon/verde_smc.bin)
  * Radeon HD 7700 series SMC microcode (radeon/VERDE_smc.bin)
 From firmware-atheros:
  * Atheros AR3012 rev 01020001 patch (ar3k/AthrBT_0x01020001.dfu)
  * Atheros AR3012 rev 01020200 patch (ar3k/AthrBT_0x01020200.dfu)
  * Atheros AR3012 rev 01020201 patch (ar3k/AthrBT_0x01020201.dfu)
  * Atheros AR3012 rev 11020000 patch (ar3k/AthrBT_0x11020000.dfu)
  * Atheros AR3012 rev 31010000 patch (ar3k/AthrBT_0x31010000.dfu)
  * Atheros AR3012 rev 41020000 patch (ar3k/AthrBT_0x41020000.dfu)
  * Atheros AR3012 rev 01020001 config (ar3k/ramps_0x01020001_26.dfu)
  * Atheros AR3012 rev 01020200 26 MHz config
    (ar3k/ramps_0x01020200_26.dfu)
  * Atheros AR3012 rev 01020200 40 MHz config
    (ar3k/ramps_0x01020200_40.dfu)
  * Atheros AR3012 rev 01020201 26 MHz config
    (ar3k/ramps_0x01020201_26.dfu)
  * Atheros AR3012 rev 01020201 40 MHz config
    (ar3k/ramps_0x01020201_40.dfu)
  * Atheros AR3012 rev 11020000 config (ar3k/ramps_0x11020000_40.dfu)
  * Atheros AR3012 rev 31010000 config (ar3k/ramps_0x31010000_40.dfu)
  * Atheros AR3012 rev 41020000 config (ar3k/ramps_0x41020000_40.dfu)
  * Atheros AR5523 firmware (ar5523.bin)
  * Atheros AR7010 rev 1.0 firmware (ar7010.fw)
  * Atheros AR7010 rev 1.1 firmware (ar7010_1_1.fw)
  * Atheros AR9170 firmware (ar9170.fw)
  * Atheros AR9271 firmware (ar9271.fw)
  * Atheros AR3011 firmware (ath3k-1.fw)
  * ath6k/AR6003.1/hw2.1.1/athwlan.bin
  * ath6k/AR6003.1/hw2.1.1/bdata.SD31.bin
  * ath6k/AR6003.1/hw2.1.1/bdata.SD32.bin
  * ath6k/AR6003.1/hw2.1.1/bdata.WB31.bin
  * ath6k/AR6003.1/hw2.1.1/data.patch.bin
  * ath6k/AR6003.1/hw2.1.1/endpointping.bin
  * ath6k/AR6003.1/hw2.1.1/otp.bin
  * ath6k/AR6003/hw1.0/athwlan.bin.z77
  * ath6k/AR6003/hw1.0/bdata.SD31.bin
  * ath6k/AR6003/hw1.0/bdata.SD32.bin
  * ath6k/AR6003/hw1.0/bdata.WB31.bin
  * ath6k/AR6003/hw1.0/data.patch.bin
  * ath6k/AR6003/hw1.0/otp.bin.z77
  * ath6k/AR6003/hw2.0/athwlan.bin.z77
  * ath6k/AR6003/hw2.0/bdata.SD31.bin
  * ath6k/AR6003/hw2.0/bdata.SD32.bin
  * ath6k/AR6003/hw2.0/bdata.WB31.bin
  * ath6k/AR6003/hw2.0/data.patch.bin
  * ath6k/AR6003/hw2.0/otp.bin.z77
  * ath6k/AR6003/hw2.1.1/athwlan.bin
  * ath6k/AR6003/hw2.1.1/bdata.SD31.bin
  * ath6k/AR6003/hw2.1.1/bdata.SD32.bin
  * ath6k/AR6003/hw2.1.1/bdata.WB31.bin
  * ath6k/AR6003/hw2.1.1/data.patch.bin
  * ath6k/AR6003/hw2.1.1/endpointping.bin
  * ath6k/AR6003/hw2.1.1/fw-2.bin
  * ath6k/AR6003/hw2.1.1/fw-3.bin
  * ath6k/AR6003/hw2.1.1/otp.bin
  * ath6k/AR6004/hw1.2/bdata.bin
  * ath6k/AR6004/hw1.2/fw-2.bin
  * ath6k/AR6004/hw1.3/bdata.bin
  * ath6k/AR6004/hw1.3/fw-3.bin
  * Atheros AR7010 firmware, version 1.3 (htc_7010.fw)
  * Atheros AR9271 firmware, version 1.3 (htc_9271.fw)
 From firmware-bnx2x:
   * Broadcom NetXtreme II 10Gb 57710, version 5.0.21.0
    (bnx2x-e1-5.0.21.0.fw)
  * Broadcom NetXtreme II 10Gb 57711, version 5.0.21.0
    (bnx2x-e1h-5.0.21.0.fw)
  * Broadcom NetXtreme II 10Gb 57710, version 7.0.29.0
    (bnx2x/bnx2x-e1-7.0.29.0.fw)
  * Broadcom NetXtreme II 10Gb 57711, version 7.0.29.0
    (bnx2x/bnx2x-e2-7.0.29.0.fw)
  * Broadcom NetXtreme II 10Gb 57712, version 7.0.29.0
    (bnx2x/bnx2x-e1h-7.0.29.0.fw)
  * Broadcom NetXtreme II 10Gb 57710, version 7.8.17.0
    (bnx2x/bnx2x-e1-7.8.17.0.fw)
  * Broadcom NetXtreme II 10Gb 57711, version 7.8.17.0
    (bnx2x/bnx2x-e2-7.8.17.0.fw)
  * Broadcom NetXtreme II 10Gb 57712, version 7.8.17.0
    (bnx2x/bnx2x-e1h-7.8.17.0.fw)
  * Broadcom NetXtreme II 10Gb 57710, version 7.8.19.0
    (bnx2x/bnx2x-e1-7.8.19.0.fw)
  * Broadcom NetXtreme II 10Gb 57711, version 7.8.19.0
    (bnx2x/bnx2x-e2-7.8.19.0.fw)
  * Broadcom NetXtreme II 10Gb 57712, version 7.8.19.0
    (bnx2x/bnx2x-e1h-7.8.19.0.fw)
 From firmware-brcm80211:
   * Broadcom 802.11 firmware, version 610.812 (brcm/bcm43xx-0.fw)
  * Broadcom 802.11 firmware header, version 610.812
    (brcm/bcm43xx_hdr-0.fw)
  * Broadcom BCM43143 firmware (brcm/brcmfmac43143-sdio.bin)
  * Broadcom BCM43241 rev 0-3 firmware (brcm/brcmfmac43241b0-sdio.bin)
  * Broadcom BCM43241 rev 4+ firmware (brcm/brcmfmac43241b4-sdio.bin)
  * Broadcom BCM4329 firmware (brcm/brcmfmac4329-sdio.bin)
  * Broadcom BCM4330 firmware (brcm/brcmfmac4330-sdio.bin)
  * Broadcom BCM4334 firmware (brcm/brcmfmac4334-sdio.bin)
  * Broadcom BCM4335 firmware (brcm/brcmfmac4335-sdio.bin)
  * Broadcom BCM43362 firmware (brcm/brcmfmac43362-sdio.bin)
  * Broadcom BCM4354 firmware (brcm/brcmfmac4354-sdio.bin)
 From firmware-cavium:
  * Cavium LiquidIO 210Nv firmware (liquidio/lio_210nv_nic.bin)
  * Cavium LiquidIO 210Sv firmware (liquidio/lio_210sv_nic.bin)
  * Cavium LiquidIO 410Nv firmware (liquidio/lio_410nv_nic.bin)
  From firmware-intel-sound:
  * Intel "Broadwell" SST DSP firmware, version 8.4.1.77
    (intel/IntcSST2.bin)
  * Intel "Broxton" SST DSP firmware, version 9.22.00.1118
    (intel/dsp_fw_bxtn_v1118.bin, intel/dsp_fw_bxtn.bin)
  * Intel "Kabylake" SST DSP firmware, version 9.21.00.1037
    (intel/dsp_fw_kbl_v1037.bin, intel/dsp_fw_kbl.bin)
  * Intel "Skylake SST DSP firmware, version 8.20.00.958
    (intel/dsp_fw_release_v958.bin, intel/dsp_fw_release.bin)
  * Intel "Bay Trail" SST DSP firmware
    (intel/fw_sst_0f28.bin-48kHz_i2s_master)
  * Intel "Bay Trail" SST DSP firmware (intel/fw_sst_0f28.bin)
  * Intel "Cherry Trail"/"Braswell" SST DSP firmware, version
    01.0B.02.02 (intel/fw_sst_22a8.bin)
 From firmware-iwlwifi:
  * Intel Wireless 3160 rev 10 Bluetooth firmware patch
    (intel/ibt-hw-37.7.10-fw-1.0.2.3.d.bseq)
  * Intel Wireless 7260 rev 10 WP2 B5 Bluetooth firmware path
    (intel/ibt-hw-37.7.10-fw-1.80.1.2d.d.bseq)
  * Intel Wireless 7260 rev 10 Bluetooth firmware patch
    (intel/ibt-hw-37.7.10-fw-1.80.2.3.d.bseq)
  * Intel Wireless 7260 Bluetooth firmware generic patch
    (intel/ibt-hw-37.7.bseq)
  * Intel Wireless 7265 rev 10 Bluetooth firmware generic patch
    (intel/ibt-hw-37.8.10-fw-1.10.2.27.d.bseq)
  * Intel Wireless 7265 Bluetooth firmware generic patch
    (intel/ibt-hw-37.8.bseq)
  * Intel Wireless 100 firmware, version 39.31.5.1 (iwlwifi-100-5.ucode)
  * Intel Wireless 105 firmware, version 18.168.6.1
    (iwlwifi-105-6.ucode)
  * Intel Wireless 135 firmware, version 18.168.6.1
    (iwlwifi-135-6.ucode)
  * Intel Wireless 1000 firmware, version 39.31.5.1
    (iwlwifi-1000-5.ucode)
  * Intel Wireless 2200 firmware, version 18.168.6.1
    (iwlwifi-2000-6.ucode)
  * Intel Wireless 2230 firmware, version 18.168.6.1
    (iwlwifi-2030-6.ucode)
  * Intel Wireless 3160 firmware, version 22.1.7.0
    (iwlwifi-3160-7.ucode)
  * Intel Wireless 3160 firmware, version 22.24.8.0
    (iwlwifi-3160-8.ucode)
  * Intel Wireless 3160 firmware, version 23.214.9.0
    (iwlwifi-3160-9.ucode)
  * Intel Wireless 3945 firmware, version 15.32.2.9
    (iwlwifi-3945-2.ucode)
  * Intel Wireless 4965 firmware, version 228.61.2.24
    (iwlwifi-4965-2.ucode)
  * Intel Wireless 5100, 5300 and 5350 firmware, version 8.24.2.12
    (iwlwifi-5000-2.ucode)
  * Intel Wireless 5100, 5300 and 5350 firmware, version 8.83.5.1
    (iwlwifi-5000-5.ucode)
  * Intel Wireless 5150 firmware, version 8.24.2.2
    (iwlwifi-5150-2.ucode)
  * Intel Wireless 6000 firmware, version 9.221.4.1
    (iwlwifi-6000-4.ucode)
  * Intel Wireless 6005/6205 firmware, version 17.168.5.3
    (iwlwifi-6000g2a-5.ucode)
  * Intel Wireless 6005/6205 firmware, version 18.168.6.1
    (iwlwifi-6000g2a-6.ucode)
  * Intel Wireless 6030 firmware, version 18.168.6.1
    (iwlwifi-6000g2b-6.ucode)
  * Intel Wireless 6250 firmware, version 9.201.4.1
    (iwlwifi-6050-4.ucode)
  * Intel Wireless 6250 firmware, version 41.28.5.1
    (iwlwifi-6050-5.ucode)
  * Intel Wireless 7260 firmware, version 22.1.7.0
    (iwlwifi-7260-7.ucode)
  * Intel Wireless 7260 firmware, version 22.24.8.0
    (iwlwifi-7260-8.ucode)
  * Intel Wireless 7260 firmware, version 23.214.9.0
    (iwlwifi-7260-9.ucode)
  * Intel Wireless 7265 firmware, version 22.24.8.0
    (iwlwifi-7265-8.ucode)
  * Intel Wireless 7265 firmware, version 23.214.9.0
    (iwlwifi-7265-9.ucode)
 From firmware-realtek:
  * Realtek RTL8192E boot code (RTL8192E/boot.img)
  * Realtek RTL8192E init data (RTL8192E/data.img)
  * Realtek RTL8192E main code (RTL8192E/main.img)
  * Realtek RTL8192SU firmware, version 902B (RTL8192SU/rtl8192sfw.bin)
  * Realtek RTL8105E-1 firmware (rtl_nic/rtl8105e-1.fw)
  * Realtek RTL8106E-1 firmware, version 0.0.1 (rtl_nic/rtl8106e-1.fw)
  * Realtek RTL8106E-2 firmware, version 0.0.1 (rtl_nic/rtl8106e-2.fw)
  * Realtek RTL8111D-1/RTL8168D-1 firmware (rtl_nic/rtl8168d-1.fw)
  * Realtek RTL8111D-2/RTL8168D-2 firmware (rtl_nic/rtl8168d-2.fw)
  * Realtek RTL8168E-1 firmware (rtl_nic/rtl8168e-1.fw)
  * Realtek RTL8168E-2 firmware (rtl_nic/rtl8168e-2.fw)
  * Realtek RTL8168E-3 firmware, version 0.0.4 (rtl_nic/rtl8168e-3.fw)
  * Realtek RTL8168F-1 firmware, version 0.0.5 (rtl_nic/rtl8168f-1.fw)
  * Realtek RTL8168F-2 firmware, version 0.0.4 (rtl_nic/rtl8168f-2.fw)
  * Realtek RTL8168G-1 firmware, version 0.0.3 (rtl_nic/rtl8168g-1.fw)
  * Realtek RTL8168G-2 firmware, version 0.0.1 (rtl_nic/rtl8168g-2.fw)
  * Realtek RTL8168G-3 firmware, version 0.0.1 (rtl_nic/rtl8168g-3.fw)
  * Realtek RTL8402-1 firmware, version 0.0.1 (rtl_nic/rtl8402-1.fw)
  * Realtek RTL8411-1 firmware, version 0.0.3 (rtl_nic/rtl8411-1.fw)
  * Realtek RTL8411-2 firmware, version 0.0.1 (rtl_nic/rtl8411-2.fw)
  * Realtek RTL8188EE firmware (rtlwifi/rtl8188efw.bin)
  * Realtek RTL8188EU firmware (rtlwifi/rtl8188eufw.bin)
  * Realtek RTL8192CE/RTL8188CE firmware, version 4.816.2011
    (rtlwifi/rtl8192cfw.bin)
  * Realtek RTL8192CE/RTL8188CE B-cut firmware, version 4.816.2011
    (rtlwifi/rtl8192cfwU_B.bin)
  * Realtek RTL8188CE A-cut firmware, version 4.816.2011
    (rtlwifi/rtl8192cfwU.bin)
  * Realtek RTL8192CU/RTL8188CU UMC A-cut firmware
    (rtlwifi/rtl8192cufw_A.bin)
  * Realtek RTL8192CU/RTL8188CU UMC B-cut firmware
    (rtlwifi/rtl8192cufw_B.bin)
  * Realtek RTL8192CU/RTL8188CU TMSC firmware
    (rtlwifi/rtl8192cufw_TMSC.bin)
  * Realtek RTL8192CU/RTL8188CU fallback firmware
    (rtlwifi/rtl8192cufw.bin)
  * Realtek RTL8192DE firmware, version 4.816.2011
    (rtlwifi/rtl8192defw.bin)
  * Realtek RTL8192SE/RTL8191SE firmware, version 4.816.2011
    (rtlwifi/rtl8192sefw.bin)
  * Realtek RTL8192SU/RTL8712U firmware (rtlwifi/rtl8712u.bin)
  * Realtek RTL8723AU rev B with-Bluetooth firmware
    (rtlwifi/rtl8723aufw_B.bin)
  * Realtek RTL8723AU rev B no-Bluetooth firmware
    (rtlwifi/rtl8723aufw_B_NoBT.bin)
  * Realtek RTL8723BE firmware (rtlwifi/rtl8723befw.bin)
  * Realtek RTL8723AE rev B firmware (rtlwifi/rtl8723fw_B.bin)
  * Realtek RTL8723AE rev A firmware (rtlwifi/rtl8723fw.bin)
  * Realtek RTL8821AE firmware (rtlwifi/rtl8821aefw.bin)
