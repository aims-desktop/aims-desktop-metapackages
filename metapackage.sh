#!/bin/bash

# personal script: fragile!
# run in ~/src/ on snapperkob

# Usage: ./metapackage.sh APPNAME DEPENDENCIES
# DEPENDENCIES is a comma-separated list: NO SPACES

export APPNAME=$1
export DEPS=$@
export DEPENDENCIES=`echo $DEPS | sed -e "s/${APPNAME} //" -e 's/ /, /g'`
export EDITOR=/usr/bin/vim

mkdir $APPNAME-0.1
tar czf ${APPNAME}_0.1.tar.gz $APPNAME-0.1
cd $APPNAME-0.1
dh_make -s -n -f ../${APPNAME}_0.1.tar.gz
rm debian/*.ex debian/*.EX debian/README.Debian debian/README.source debian/docs debian/README
sed -ie 's#Homepage:.*#Homepage: https://launchpad.net/~aims#' debian/control
sed -ie "s/misc:Depends}/misc:Depends}, ${DEPENDENCIES}/" debian/control
$EDITOR debian/control
rm debian/controle
sed -ie 's/unstable/trusty/' debian/changelog
rm debian/changeloge
dch
debuild -k'Jan Groenewald (Www.aims.ac.za) <jan@aims.ac.za>' -S -sa
cd ..
dput ppa:aims/aims-desktop ${APPNAME}_0.1_source.changes
