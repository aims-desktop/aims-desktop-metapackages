#!/bin/bash

# TODO
# check IP address is 10.1 and set ZA proxy
echo "Set proxy and press enter"
read PROXY

# Disable LTS upgrade prompts
sed -i 's/=lts/=never/' /etc/update-manager/release-upgrades

# Update all software
echo "Updating package lists..."
apt-get -qq update
apt-get -y install aims-restricted-extras
aims-restricted-extras-setup
apt-get -y dist-upgrade
apt-get -y clean
aims-jupyter-install.sh
aims-desktop-acroread-installer-raring
apt-get -y clean

# check for drivers


