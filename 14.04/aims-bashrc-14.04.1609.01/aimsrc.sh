# Various AIMS settings

#[ -x /usr/bin/lesspipe ] && eval "$(lesspipe)"
export EDITOR='/usr/bin/gedit'
#export PERL_BADLANG=0
alias python='python -Q new'
export HISTCONTROL=ignoredups
export HISTFILESIZE=8192
export RSYNC_RSH=ssh
export IGNOREEOF=0
eval `dircolors -b`
alias l='ls -lap'
# colourful grep
alias grep='grep --color' 
alias egrep='egrep --color' 
# colourful diff
alias diff='colordiff'
# man pages in colour
# colourful gcc # colorgcc must be installed
# few enough people use gcc intensively for this not to be disruptive
export CC='/usr/bin/colorgcc'
alias gcc='colorgcc'
# This is disabled -- it breaks when used from /etc/profile.d/
# Use it from ~/.bashrc instead.
#export LESS_TERMCAP_mb=$'\E[01;31m'
#export LESS_TERMCAP_md=$'\E[01;31m'
#export LESS_TERMCAP_me=$'\E[0m'
#export LESS_TERMCAP_se=$'\E[0m'
#export LESS_TERMCAP_so=$'\E[01;44;33m'
#export LESS_TERMCAP_ue=$'\E[0m'
#export LESS_TERMCAP_us=$'\E[01;32m'
# enable bash completion in interactive shells (this instance added by AIMS install script)
#if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
#        . /etc/bash_completion
#fi
# EOF /etc/bash.aimsrc
