#!/bin/bash

# This script makes it easier to install science packages on
# an fresh Ubuntu 12.04 (precise) installation. It sets repositories
# up and installs a meta-package "aims-desktop" which depends on many
# packages in various repositories, including scientific software, LaTeX
# tools, desktop applications, and codecs.

# The aim is to send other Linux administrators or users only 5/6 commands:
#sudo apt-add-repository ppa:aims/aims-desktop
#sudo apt-get update
#sudo apt-get install aims-desktop-setup
#sudo aims-desktop-setup
#sudo forticlientsslvpn-installer # only works at AIMS (enables AIMS LAN repository)

# For the Ubuntu installation, we prefer to install one partition (and swap=RAM)
# if there is <= 64G available for Linux, else we separate out /=16-32G and /home=REST

##################################
# Disable apport error reporting #
##################################
echo "Disabling error reporting in /etc/default/apport"
sed -ie 's/enabled=1/enabled=0/' /etc/default/apport

# At AIMS we are probably also doing the Ubuntu installation ourselves, and using our proxy settings
# Now disabled as our proxy is transparent
# Set the AIMS proxy
# TODO add -p or some flag to set below when at AIMS 
#export http_proxy=http://proxy.aims.ac.za:3128/
#export https_proxy=http://proxy.aims.ac.za:3128/

echo "This script expects a standard /etc/apt/sources.list file and modifies it to disable deb-src lines and enable all deb lines, i.e. main, restricted, universe, multiverse, security, backports, partner, and extra."
echo "It also adds ppa:aims/sagemath, VideoLAN, and Google" #, google-talkplugin, and google-musicmanager."

########################
# Set the repositories #
########################

# Enable all deb lines for packages trusty, trusty-updates, security updates,
# trusty-backports, from the sections main, restricted, universe, multiverse,
# Canonical partner and extra repositories from thrid party developers,
# but disable source packages, and disable cdrom lines
sed -ie 's/# deb http/deb http/' /etc/apt/sources.list
sed -ie 's/^deb-src http/# deb-src http/' /etc/apt/sources.list
sed -ie 's/^deb cdrom/# deb cdrom/' /etc/apt/sources.list
rm -f /etc/apt/sources.liste

# Enable Videolan
if [ -f /etc/apt/sources.list.d/medibuntu.list ]; then
	# Medibuntu discontinued and VideoLAN taking over
	echo "Disabling Medibuntu and enabling VideoLAN"
	echo "https://launchpad.net/medibuntu/+announcement/11951"
	# Remove even disabled sources --- medibuntu is dead
	rm -f /etc/apt/sources.list.d/medibuntu.list*
fi;
echo "Removing medibuntu-keyring if installed."
dpkg -l medibuntu-keyring && apt-get -y purge medibuntu-keyring
# Get key if it probably has not been gotten before; there is no videolan.list* at all
echo "Enabling VideoLAN."
if [ ! -f /etc/apt/sources.list.d/videolan.list ]; then
	wget -O - http://download.videolan.org/pub/ubuntu/videolan-apt.asc|sudo apt-key add -
fi;
# Replace previous ubuntu/precise format in the file with ubuntu/stable
# It is the same package and there is no shortcut folder for ubuntu/trusty
# at http://download.videolan.org/pub/ubuntu on 21 Apr 2014 (3 days after trusty released)
cat > /etc/apt/sources.list.d/videolan.list << EOF
# http://www.videolan.org/developers/libdvdcss.html 

deb http://download.videolan.org/pub/ubuntu/stable/ /
#deb-src http://download.videolan.org/pub/ubuntu/stable/ /
EOF

# Enable AIMS PPAs ( http://www.launchpad.net/~aims ), but without source packages
echo "Enabling AIMS desktop and sagemath sources (for trusty)."
rm -f /etc/apt/sources.list.d/aims-*-precise.list*
if [ ! -f /etc/apt/sources.list.d/aims-aims-desktop-trusty.list ]; then
	# Bug in apt-add-repository -- adds only commented deb-src line?
	# Perhaps looking at distUpgrade or Save files
	# Need to run this though to get the key. Then just do it manually, overrinding:
	apt-add-repository -y ppa:aims/aims-desktop
	cat > /etc/apt/sources.list.d/aims-aims-desktop-trusty.list << EOF
deb http://ppa.launchpad.net/aims/aims-desktop/ubuntu trusty main
# deb-src http://ppa.launchpad.net/aims/aims-desktop/ubuntu trusty main
EOF
	fi;
if [ ! -f /etc/apt/sources.list.d/aims-sagemath-trusty.list ]; then
	apt-add-repository -y ppa:aims/sagemath
	cat > /etc/apt/sources.list.d/aims-sagemath-trusty.list << EOF
deb http://ppa.launchpad.net/aims/sagemath/ubuntu trusty main
# deb-src http://ppa.launchpad.net/aims/sagemath/ubuntu trusty main
EOF
fi;
# Enable jtaylor/ipython PPA for IPython2
echo "Enabling jtaylor/ipython sources for IPython2"
if [ ! -f /etc/apt/sources.list.d/jtaylor-ipython-trusty.list ]; then
	apt-add-repository -y ppa:jtaylor/ipython
	cat > /etc/apt/sources.list.d/jtaylor-ipython-trusty.list << EOF
deb http://ppa.launchpad.net/jtaylor/ipython/ubuntu trusty main
# deb-src http://ppa.launchpad.net/jtaylor/ipython/ubuntu trusty main
EOF
fi;
# Enable thefanclub PPA for grive-tools (Google Drive)
echo "Enabling thefanclub sources."
if [ ! -f /etc/apt/sources.list.d/thefanclub-grive-tools-trusty.list ]; then
	add-apt-repository -y ppa:thefanclub/grive-tools
	cat > /etc/apt/sources.list.d/thefanclub-grive-tools-trusty.list << EOF
deb http://ppa.launchpad.net/thefanclub/grive-tools/ubuntu trusty main
# deb-src http://ppa.launchpad.net/thefanclub/grive-tools/ubuntu trusty main
EOF
fi;

# possible PPA candidate: atareao/atareao (some indicators? other?)

# Clean up sources.list.d a little bit
rm -f /etc/apt/sources.list.d/*.list.save
rm -f /etc/apt/sources.list.d/*.list.distUpgrade

# moved to separate script: google-repos (downloads fail too often!)
#	# Enable google-musicmanager and google-talkplugin
#	cat > /etc/apt/sources.list.d/google-talkplugin.list << EOF
#	### THIS FILE IS AUTOMATICALLY CONFIGURED ###
#	# You may comment out this entry, but any other modifications may be lost.
#	deb http://dl.google.com/linux/talkplugin/deb/ stable main
#	EOF
#	cat > /etc/apt/sources.list.d/google-musicmanager.list << EOF
#	### THIS FILE IS AUTOMATICALLY CONFIGURED ###
#	# You may comment out this entry, but any other modifications may be lost.
#	deb http://dl.google.com/linux/musicmanager/deb/ stable main
#	EOF
#	# Get necessary keys for Google repositories
#	#wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
#	apt-key adv --keyserver keyserver.ubuntu.com --recv-keys A040830F7FAC5991

#########################################
# Update the list of available software #
#########################################
apt-get update

# Seeds for packages that ask interactive questions:
# The next upgrade of msttcorefonts wants to confirm the license
cat > /tmp/msttcorefonts.seed <<EOF
ttf-mscorefonts-installer	msttcorefonts/dlurl	string
ttf-mscorefonts-installer	msttcorefonts/accepted-mscorefonts-eula	boolean true
ttf-mscorefonts-installer	msttcorefonts/present-mscorefonts-eula	note
ttf-mscorefonts-installer	msttcorefonts/baddldir	error	
ttf-mscorefonts-installer	msttcorefonts/dldir	string
ttf-mscorefonts-installer	msttcorefonts/error-mscorefonts-eula	error
EOF
debconf-set-selections /tmp/msttcorefonts.seed
# WHAT IS STATUS OF AUTO SECURITY UPDATES ON AIMS-DESKTOP!? MUST CONFIRM
# The next upgrade of unattended-upgrades should enable automatic upgrades
cat > /tmp/unattended-upgrades.seed <<EOF
unattended-upgrades	unattended-upgrades/enable_auto_updates	boolean	true
EOF
debconf-set-selections /tmp/unattended-upgrades.seed
# Above does not work?
cat > /etc/apt/apt.conf.d/20auto-upgrades << EOF
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Unattended-Upgrade "1";
EOF

# update to latest versions first
apt-get -y dist-upgrade

# Rest should be done by a single package.
# Currently there is no 32bit sagemath package for 14.04 (only binary downloads).
ARCH=X`uname -m`
if [ ${ARCH} = X"i686" -o ${ARCH} = X"i586" ]; then
	# aims-desktop except aims-{desktop,science}-standard; aims-science-standard except sagemath-upstream-binary
	apt-get -y install aims-science-minimal aims-latex-standard aims-utils-standard aims-media-standard aims-language-standard build-essential python-visual gnuplot python-gnuplot python-networkx python-rpy2 r-recommended r-cran-plyr r-cran-reshape2 r-cran-ggplot2 rstudio-upstream-deb cython git python-tables python-pandas python-sparse python-sklearn python-sympy;
	# sagemath-upstream-binary dependencies
	apt-get -y install libc6 libexpat1 libfontconfig1 libfreetype6 libgcc1 libgd2-xpm libgmp10 libgmpxx4ldbl libgsl0ldbl libjpeg-turbo8 libmpfr4 libpng12-0 libppl13 libppl-c4 libpython2.7 libreadline6 libsqlite3-0 libssl1.0.0 libstdc++6 libtinfo5 libx11-6 libxau6 libxcb1 libxdmcp6 libxpm4 zlib1g
	# sagemath-upstream-binary (strong) recommendations
	apt-get -y install openssl build-essential gfortran libgfortran3 openssh-client imagemagick ffmpeg dvipng texlive texlive-pictures icedtea-7-plugin;
  	echo "Please install sagemath from source from http://www.sagemath.org/download-source.html";
else
	apt-get -y install aims-desktop;
fi

# Can save space, but if something failed, e.g. timeout during last package download
# You will heave to RE-download all the other packages too. Best leave without this.
# What is the time after which this will be automatically cleared? Perhaps cron.daily/apt??
# apt-get clean

# annoying nautilus-dropbox messages
rm -f /var/lib/update-notifier/user.d/dropbox-*start-required

# Skype fails to install in UCK. Only install skype if we are not in UCK creating an ISO
mount | grep remaster-root || apt-get -y install skype

# Sometimes hangs while trying to download from google; enabled, but candidate for disabling if causing trouble
echo "Enabling google repos, and installing talkplugin. This sometimes hangs, if so try it manually"
echo "/usr/bin/google-repos-setup"
/usr/bin/google-repos-setup
echo "apt-get -y install google-talkplugin"
apt-get -y install google-talkplugin

# Another google: fix flash for chromium (aims-media-standard depends on pepperflashplugin-nonfree)
# Suddenly sudo -E does not honour proxy; so source a manual proxy setting from /etc/environment
which update-pepperflashplugin-nonfree && source /etc/environment && update-pepperflashplugin-nonfree --install

#echo "Now do sudo apt-get install aims-desktop"
#echo "Or sudo apt-get install aims-desktop-minimal for scipy only (no sage) for a faster install."

