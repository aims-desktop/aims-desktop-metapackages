umask 022
#pip3 install -I path.py==7.7.1
pip3 install widgetsnbextension
sed -i.bak 's/MARKER_EXPR()/MARKER_EXPR("")/' /usr/local/lib/python3.4/dist-packages/packaging/requirements.py
pip3 install jupyter zmq
# python-networkx and python3-networkx contain a duplicate file, get networkx for python3 manually:
pip3 install networkx
