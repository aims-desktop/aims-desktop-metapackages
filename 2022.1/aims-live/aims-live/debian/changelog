aims-live (2:2022.6) bullseye; urgency=medium

  * Bump epoch after PPA problem

 -- Jonathan Carter <jcc@debian.org>  Wed, 19 Apr 2023 11:59:43 +0200

aims-live (1:2022.5) bullseye; urgency=medium

  * Update calamares branding to 2022.2 

 -- Jonathan Carter <jcc@debian.org>  Wed, 19 Apr 2023 09:30:05 +0200

aims-live (1:2022.4) bullseye; urgency=medium

  * Fix source URI in source.list generator for bullseye-security

 -- Jonathan Carter <jonathan@aims.ac.za>  Thu, 05 Aug 2021 12:29:58 +0200

aims-live (1:2022.3) bullseye; urgency=medium

  * Update sources.list (source-final) with new security syntax

 -- Jan Groenewald <jan@aims.ac.za>  Tue, 25 May 2021 15:00:26 +0200

aims-live (1:2022.2) bullseye; urgency=medium

  * Replace calamares scripts s/buster/bullseye/g
  * Remove some conflicts preventing sagemath to be installed

 -- Jonathan Carter <jonathan@aims.ac.za>  Wed, 24 Mar 2021 16:25:16 +0200

aims-live (1:2022.1) bullseye; urgency=medium

  * Initial bullseye PPA upload

 -- Jonathan Carter <jonathan@aims.ac.za>  Tue, 23 Mar 2021 11:04:55 +0200

aims-live (1:2020.17) unstable; urgency=medium

  * Update release version to 2020.2

 -- Jonathan Carter <jcc@debian.org>  Mon, 07 Sep 2020 12:40:59 +0200

aims-live (1:2020.16) buster; urgency=medium

  * Extend default timeout from 30s to 300s for sources-installtime

 -- Jonathan Carter <jcc@debian.org>  Wed, 28 Aug 2019 11:50:37 +0000

aims-live (1:2020.15) buster; urgency=medium

  * Remove large doc packages (singular-doc, sagemath-doc-en)

 -- Jonathan Carter <jcc@debian.org>  Mon, 19 Aug 2019 12:58:47 +0200

aims-live (1:2020.14) buster; urgency=medium

  * Add better access defaults for initramfs

 -- Jonathan Carter <jcc@debian.org>  Wed, 03 Jul 2019 12:13:28 +0000

aims-live (1:2020.13) buster; urgency=medium

  * Don't update package lists in live session

 -- Jonathan Carter <jcc@debian.org>  Tue, 02 Jul 2019 09:10:35 +0000

aims-live (1:2020.12) buster; urgency=medium

  * Update calamares slide
  * Add memtest86+ to live depends

 -- Jonathan Carter <jcc@debian.org>  Thu, 20 Jun 2019 09:55:46 +0200

aims-live (1:2020.11) buster; urgency=medium

  * Add trusted=yes for local install media

 -- Jonathan Carter <jcc@debian.org>  Wed, 12 Jun 2019 11:56:15 +0200

aims-live (1:2020.10) buster; urgency=medium

  * Don't add calamares desktop icon, it doesn't work with
    desktop shell extension
  * Update calamares branding/boot entries
  * Update branding images

 -- Jonathan Carter <jcc@debian.org>  Fri, 26 Apr 2019 08:45:09 +0000

aims-live (1:2020.9) buster; urgency=medium

  * Add script to add calamares icon on desktop

 -- Jonathan Carter <jcc@debian.org>  Thu, 25 Apr 2019 13:38:10 +0000

aims-live (1:2020.8) buster; urgency=medium

  * Attempt bootloader name fix

 -- Jonathan Carter <jonathan@aims.ac.za>  Mon, 25 Feb 2019 07:23:41 +0000

aims-live (1:2020.7) buster; urgency=medium

  * Update sagemath icon name in gsettings

 -- Jonathan Carter <jonathan@aims.ac.za>  Tue, 12 Feb 2019 05:15:12 +0000

aims-live (1:2020.6) buster; urgency=medium

  * Remove python-sagetex from conflicts
  * Remove standards version field
  * Update compat to debhelper-compat=12
  * Fix maintainer field

 -- Jonathan Carter <jonathan@aims.ac.za>  Mon, 11 Feb 2019 12:34:36 +0000

aims-live (1:2020.5) buster; urgency=medium

  * Bump installtime sources to Buster

 -- Kyle Robbertze <kyle@aims.ac.za>  Fri, 25 Jan 2019 10:16:32 +0200

aims-live (1:2020.4) buster; urgency=medium

  * Remove openswapluksconfig from calamares modules
  * Bump epoch to match stretch package

 -- Jonathan Carter <jonathan@aims.ac.za>  Wed, 16 Jan 2019 12:04:58 +0200

aims-live (2020.3) buster; urgency=medium

  * Add fix to start calamares under xwayland
  * Update calamares branding version to 2020.1

 -- Jonathan Carter <jonathan@aims.ac.za>  Wed, 09 Jan 2019 10:06:31 +0200

aims-live (2020.3) buster; urgency=medium

  * Add world-clocks to AIMS live

 -- Jan Groenewald <jan@aims.ac.za>  Mon, 01 Oct 2018 15:28:36 +0200

aims-live (2020.2) buster; urgency=medium

  * Update calamares modules path
  * Replace calamares services job with services-systemd
  * Update calamares live medium location
  * Update sources.list generation (s/stretch/buster)
  * Add mount.conf and machineid.conf for calamares

 -- Jonathan Carter <jonathan@aims.ac.za>  Mon, 01 Oct 2018 11:52:15 +0200

aims-live (2020.1) buster; urgency=medium

  * Add Jan Groenewald to maintainers
  * Update compat to level 11
  * Update standards version to 4.2.1
  * Update homepage
  * Add Vcs-browser fields
  * Remove shlibst substvar
  * Update copyright years and URL in debian/copyright
  * Remove unneeded splash kernel paramater update in postinst

 -- Jonathan Carter <jonathan@aims.ac.za>  Fri, 28 Sep 2018 10:23:39 +0200

aims-live (2017.2.20180924.1) stretch; urgency=medium

  * Add plymouthcfg to calamares/settings

 -- Jonathan Carter <jcc@debian.org>  Tue, 25 Sep 2018 12:36:22 +0200

aims-live (2017.2.20180919.2) stretch; urgency=medium

  * Add StartupWMClass to install-aims-desktop to avoid duplicate
    calamares panel entry

 -- Jonathan Carter <jcc@debian.org>  Wed, 19 Sep 2018 13:50:06 +0200

aims-live (2017.2.20180919.1) stretch; urgency=medium

  * Add divert to remove default calamares desktop entry from live session

 -- Jonathan Carter <jcc@debian.org>  Wed, 19 Sep 2018 13:38:00 +0200

aims-live (2017.2.20180913.2) stretch; urgency=medium

  * Fix typo and prettify sources-final

 -- Jan Groenewald <jan@aims.ac.za>  Tue, 25 Sep 2018 16:32:24 +0200

aims-live (2017.2.20180913.1) stretch; urgency=medium

  * Fix splash option in /etc/default/grub

 -- Jan Groenewald <jan@aims.ac.za>  Thu, 13 Sep 2018 14:42:38 +0200

aims-live (2017.2.20180912.1) unstable; urgency=medium

  * Update sources-final to use deb.debian.org

 -- Jonathan Carter <jcc@debian.org>  Wed, 12 Sep 2018 09:44:30 +0200

aims-live (2017.2.20180910.2) stretch; urgency=medium

  * Fix permissions

 -- Jonathan Carter <jcc@debian.org>  Mon, 10 Sep 2018 11:26:01 +0200

aims-live (2017.2.20180910.1) stretch; urgency=medium

  * Move grub update snippet here.

 -- Jonathan Carter <jcc@debian.org>  Mon, 10 Sep 2018 11:03:46 +0200

aims-live (2017.2.20180906.3) stretch; urgency=medium

  * Add md5sumchecker

 -- Jonathan Carter <jonathan@aims.ac.za>  Thu, 06 Sep 2018 14:24:40 +0200

aims-live (2017.2.20180906.2) stretch; urgency=medium

  * No changes upload

 -- Jonathan Carter <jcarter@linux.com>  Thu, 06 Sep 2018 12:47:16 +0200

aims-live (2017.2.20180906.1) stretch; urgency=medium

  * Remove spyder from favourites

 -- Jan Groenewald <jan@aims.ac.za>  Thu, 06 Sep 2018 10:03:25 +0200

aims-live (2017.2.20180425.4) unstable; urgency=medium

  * Add final module for encrypted disk fix

 -- Jonathan Carter <jcc@debian.org>  Wed, 25 Apr 2018 14:13:07 +0200

aims-live (2017.2.20180425.3) unstable; urgency=medium

  * Calamares settings fixes

 -- Jonathan Carter <jcc@debian.org>  Wed, 25 Apr 2018 12:46:07 +0200

aims-live (2017.2.20180425.2) unstable; urgency=medium

  * Fix enable-locales module installation

 -- Jonathan Carter <jcc@debian.org>  Wed, 25 Apr 2018 12:35:47 +0200

aims-live (2017.2.20180425.1) unstable; urgency=medium

  * Add configuration needed for encrypted volumes

 -- Jonathan Carter <jcc@debian.org>  Wed, 25 Apr 2018 11:25:50 +0200

aims-live (2017.2.20180424.1) stretch; urgency=medium

  * Add dependency on hashcheck

 -- Jonathan Carter <jcc@debian.org>  Tue, 24 Apr 2018 10:26:59 +0200

aims-live (2017.1.20170531.1) stretch; urgency=medium

  * Update Tilix entry in live session

 -- Jonathan Carter <jcarter@linux.com>  Wed, 31 May 2017 08:45:50 +0200

aims-live (2017.1.20170426.1) stretch; urgency=medium

  * Conflict with cups-browsed which uses 100% CPU in some cases

 -- Jan Groenewald <jan@aims.ac.za>  Wed, 26 Apr 2017 09:34:45 +0200

aims-live (2017.1.20170405.1) stretch; urgency=medium

  * Disable netinstall until network related issues are fixed

 -- Jonathan Carter <jcarter@linux.com>  Wed, 05 Apr 2017 09:32:31 +0200

aims-live (2017.1.20170315.14) stretch; urgency=medium

  * Remove gnome-getting-started-docs from Conflicts

 -- Jan Groenewald <jan@aims.ac.za>  Wed, 15 Mar 2017 14:22:14 +0200

aims-live (2017.1.20170221.13) stretch; urgency=medium

  * Move conflicts: gnome-getting-started-docs from aims-desktop to aims-live

 -- Jan Groenewald <jan@aims.ac.za>  Tue, 14 Mar 2017 11:46:41 +0200

aims-live (2017.1.20170221.12) stretch; urgency=medium

  * Uodate sources.list

 -- Jonathan Carter <jcarter@linux.com>  Tue, 21 Feb 2017 10:25:36 +0200

aims-live (2017.1.20170206.11) stretch; urgency=medium

  * Add netinstall module to calamares installer

 -- Kyle Robbertze <krobbertze@gmail.com>  Mon, 20 Feb 2017 16:26:54 +0200

aims-live (2017.1.20170206.10) stretch; urgency=medium

  * Remove yelp from launcher

 -- Jonathan Carter <jcarter@linux.com>  Mon, 13 Feb 2017 13:21:38 +0200

aims-live (2017.1.20170206.9) stretch; urgency=medium

  * Fix conflict entries

 -- Jonathan Carter <jcarter@linux.com>  Mon, 13 Feb 2017 12:03:36 +0200

aims-live (2017.1.20170206.8) stretch; urgency=medium

  * Disable swap by default
  * Remove explicit depends on sagemath

 -- Jonathan Carter <jcarter@linux.com>  Mon, 13 Feb 2017 09:31:54 +0200

aims-live (2017.1.20170206.7) stretch; urgency=medium

  * Depand on sagemath
    This will purposely break our installation but will help
    us find a bad conflict.

 -- Jonathan Carter <jcarter@linux.com>  Wed, 08 Feb 2017 14:46:51 +0200

aims-live (2017.1.20170206.6) stretch; urgency=medium

  * Re-add install-aims-desktop.desktop to live launcher

 -- Jonathan Carter <jcarter@linux.com>  Wed, 08 Feb 2017 13:54:14 +0200

aims-live (2017.1.20170206.5) stretch; urgency=medium

  * Remove libc-dbg as conflict (prevents sagemath installation)

 -- Jonathan Carter <jcarter@linux.com>  Wed, 08 Feb 2017 12:43:42 +0200

aims-live (2017.1.20170206.4) stretch; urgency=medium

  * Version bump after merging conflicts

 -- Jan Groenewald <jan@aims.ac.za>  Wed, 08 Feb 2017 11:33:44 +0200

aims-live (2017.1.20170206.3) stretch; urgency=medium

  * Version bump

 -- Jonathan Carter <jcarter@linux.com>  Mon, 06 Feb 2017 15:21:44 +0200

aims-live (2017.1.20170206.2) stretch; urgency=medium

  * Remove conflicts which cause removal of sagemath

 -- Jan Groenewald <jan@aims.ac.za>  Tue, 07 Feb 2017 16:57:06 +0200

aims-live (2017.1.20170206.1) stretch; urgency=medium

  * Update live launcher entries
  * Update sources.list entry

 -- Jonathan Carter <jcarter@linux.com>  Mon, 06 Feb 2017 09:05:59 +0200

aims-live (2017.1.20160131.1) stretch; urgency=medium

  * Update version references to 2017.1

 -- Jonathan Carter <jcarter@linux.com>  Tue, 31 Jan 2017 09:02:43 +0200

aims-live (2016.46) stretch; urgency=medium

  * Added conflicts to remove packages recommended by jupyter/sage

 -- Jan Groenewald <jan@aims.ac.za>  Tue, 17 Jan 2017 10:09:46 +0200

aims-live (2016.45) unstable; urgency=medium

  * Version bump to fox previous upload without deb

 -- Jan Groenewald <jan@aims.ac.za>  Fri, 16 Dec 2016 15:42:45 +0200

aims-live (2016.44) unstable; urgency=medium

  * Add few more texlive-* conflicts to reduce size

 -- Jan Groenewald <jan@aims.ac.za>  Wed, 14 Dec 2016 11:33:15 +0200

aims-live (2016.43) unstable; urgency=medium

  * Unremove plymouth

 -- Jonathan Carter <jcarter@linux.com>  Mon, 21 Nov 2016 15:20:44 +0200

aims-live (2016.42) unstable; urgency=medium

  * Replace terminator icon with terminix in panel

 -- Jonathan Carter <jcarter@linux.com>  Mon, 21 Nov 2016 11:43:50 +0200

aims-live (2016.41) unstable; urgency=medium

  * Add qtquick deps
  * Fix grey colour on installer

 -- Jonathan Carter <jcarter@linux.com>  Wed, 16 Nov 2016 10:02:55 +0200

aims-live (2016.40) unstable; urgency=medium

  * Fix qml file.

 -- Jonathan Carter <jcarter@linux.com>  Wed, 19 Oct 2016 15:30:12 +0200

aims-live (2016.39) unstable; urgency=medium

  * Update slide files.

 -- Jonathan Carter <jcarter@linux.com>  Wed, 19 Oct 2016 15:01:43 +0200

aims-live (2016.38) unstable; urgency=medium

  * Update sources.list

 -- Jonathan Carter <jcarter@linux.com>  Wed, 19 Oct 2016 13:23:50 +0200

aims-live (2016.37) unstable; urgency=medium

  * Add calculator to live launcher

 -- Jonathan Carter <jcarter@linux.com>  Wed, 05 Oct 2016 11:54:13 +0200

aims-live (2016.36) unstable; urgency=medium

  * Add sagemath to live launcher

 -- Jonathan Carter <jcarter@linux.com>  Wed, 05 Oct 2016 08:36:06 +0200

aims-live (2016.35) unstable; urgency=medium

  * Disable auto-screensaver for live session

 -- Jonathan Carter <jcarter@linux.com>  Mon, 03 Oct 2016 13:11:58 +0200

aims-live (2016.34) unstable; urgency=medium

  * Add sagemath ppa to sources.list

 -- Jonathan Carter <jcarter@linux.com>  Wed, 28 Sep 2016 15:47:03 +0200

aims-live (2016.32) unstable; urgency=medium

  * Update live postrm

 -- Jonathan Carter <jcarter@linux.com>  Mon, 12 Sep 2016 12:22:03 +0200

aims-live (2016.31) unstable; urgency=medium

  * Add system configuration to postrm

 -- Jonathan Carter <jcarter@linux.com>  Mon, 12 Sep 2016 11:45:26 +0200

aims-live (2016.30) unstable; urgency=medium

  * Fix sources.list writer

 -- Jonathan Carter <jcarter@linux.com>  Tue, 02 Aug 2016 13:50:04 +0200

aims-live (2016.29) unstable; urgency=medium

  * Extend eficonfig timeout to 600

 -- Jonathan Carter <jcarter@linux.com>  Mon, 01 Aug 2016 15:17:36 +0200

aims-live (2016.28) unstable; urgency=medium

  * Now run all actions in modules outside of chroot

 -- Jonathan Carter <jcarter@linux.com>  Mon, 01 Aug 2016 14:27:56 +0200

aims-live (2016.27) unstable; urgency=medium

  * Attempt writing sources from outside chroot

 -- Jonathan Carter <jcarter@linux.com>  Mon, 01 Aug 2016 13:44:04 +0200

aims-live (2016.26) unstable; urgency=medium

  * Add chroot statements in json files

 -- Jonathan Carter <jcarter@linux.com>  Mon, 01 Aug 2016 12:43:25 +0200

aims-live (2016.25) unstable; urgency=medium

  * Use explicit paths

 -- Jonathan Carter <jcarter@linux.com>  Mon, 01 Aug 2016 11:51:47 +0200

aims-live (2016.24) unstable; urgency=medium

  * Move packages operation upwards for calamares

 -- Jonathan Carter <jcarter@linux.com>  Wed, 27 Jul 2016 16:58:33 +0200

aims-live (2016.23) unstable; urgency=medium

  * Avoid bad exit status

 -- Jonathan Carter <jcarter@linux.com>  Wed, 27 Jul 2016 15:29:54 +0200

aims-live (2016.22) unstable; urgency=medium

  * Live fixes

 -- Jonathan Carter <jcarter@linux.com>  Wed, 27 Jul 2016 14:28:25 +0200

aims-live (2016.21) unstable; urgency=medium

  * Media chroots and efi updates

 -- Jonathan Carter <jcarter@linux.com>  Wed, 27 Jul 2016 11:59:41 +0200

aims-live (2016.20) unstable; urgency=medium

  * Update efi config

 -- Jonathan Carter <jcarter@linux.com>  Wed, 27 Jul 2016 10:59:27 +0200

aims-live (2016.19) unstable; urgency=medium

  * Partial efi support

 -- Jonathan Carter <jcarter@linux.com>  Tue, 26 Jul 2016 12:57:40 +0200

aims-live (2016.18) unstable; urgency=medium

  * Change default location

 -- Jonathan Carter <jcarter@linux.com>  Mon, 25 Jul 2016 15:23:21 +0200

aims-live (2016.17) unstable; urgency=medium

  * Fix gsettings

 -- Jonathan Carter <jcarter@linux.com>  Mon, 18 Jul 2016 15:38:18 +0200

aims-live (2016.16) unstable; urgency=medium

  * Fix gsettings

 -- Jonathan Carter <jcarter@linux.com>  Mon, 18 Jul 2016 12:05:36 +0200

aims-live (2016.15) unstable; urgency=medium

  * Temporary conflicts

 -- Jonathan Carter <jcarter@linux.com>  Tue, 14 Jun 2016 16:05:52 +0200

aims-live (2016.14) unstable; urgency=medium

  * Remove gdm hack

 -- Jonathan Carter <jcarter@linux.com>  Tue, 14 Jun 2016 12:51:16 +0200

aims-live (2016.13) unstable; urgency=medium

  * Remove tex packages

 -- Jonathan Carter <jcarter@linux.com>  Tue, 14 Jun 2016 09:45:42 +0200

aims-live (2016.12) unstable; urgency=medium

  * Move live configs up

 -- Jonathan Carter <jcarter@linux.com>  Tue, 14 Jun 2016 09:30:46 +0200

aims-live (2016.11) unstable; urgency=medium

  * Hack to get gdm running for testing

 -- Jonathan Carter <jcarter@linux.com>  Tue, 14 Jun 2016 08:29:14 +0200

aims-live (2016.10) unstable; urgency=medium

  * Add install path for dconf settings

 -- Jonathan Carter <jcarter@linux.com>  Mon, 13 Jun 2016 15:39:24 +0200

aims-live (2016.9) unstable; urgency=medium

  * Add dconf settings

 -- Jonathan Carter <jcarter@linux.com>  Mon, 13 Jun 2016 15:35:48 +0200

aims-live (2016.8) unstable; urgency=medium

  * Update installer desktop entry

 -- Jonathan Carter <jcarter@linux.com>  Mon, 13 Jun 2016 15:13:08 +0200

aims-live (2016.7) unstable; urgency=medium

  * Remove additional file

 -- Jonathan Carter <jcarter@linux.com>  Mon, 13 Jun 2016 14:45:20 +0200

aims-live (2016.6) unstable; urgency=medium

  * Fix typo

 -- Jonathan Carter <jcarter@linux.com>  Mon, 13 Jun 2016 14:06:46 +0200

aims-live (2016.5) unstable; urgency=medium

  * Work around X issue

 -- Jonathan Carter <jcarter@linux.com>  Mon, 13 Jun 2016 13:52:04 +0200

aims-live (2016.4) unstable; urgency=medium

  * Update .desktop file

 -- Jonathan Carter <jcarter@linux.com>  Mon, 13 Jun 2016 12:33:19 +0200

aims-live (2016.3) unstable; urgency=medium

  * Add nice installer icon

 -- Jonathan Carter <jcarter@linux.com>  Mon, 13 Jun 2016 12:25:11 +0200

aims-live (2016.2) unstable; urgency=medium

  * Add Calamares config

 -- Jonathan Carter <jcarter@linux.com>  Mon, 13 Jun 2016 12:02:01 +0200

aims-live (2016.1) unstable; urgency=medium

  * Initial release

 -- Jonathan Carter <jcarter@linux.com>  Wed, 01 Jun 2016 14:13:32 +0200
