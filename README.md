AIMS Desktop meta-packages
--------------------------

This repository contains meta-packages used in AIMS Desktop.

It contains:

 - aims-bashrc
 - aims-desktop
 - aims-desktop-minimal
 - aims-desktop-setup
 - aims-desktop-standard
 - aims-glib-schema
 - aims-glib-schema-laptop
 - aims-lab-auth
 - aims-language-minimal
 - aims-language-standard
 - aims-live
 - aims-media-minimal
 - aims-media-standard
 - aims-proxyrc
 - aims-restricted-extras
 - aims-science-minimal
 - aims-science-standard
 - aims-server
 - aims-utils-minimal
 - aims-utils-standard