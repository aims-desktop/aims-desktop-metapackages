# VMs & development
jonathan
kyle

# On top of plain Debian
jan-desktop
kontiki

# From early installer
yae-desktop
evansotieno (to kenya)
mateyisi (?stellenbosch)
buri (aims research centre) (updated 8 Feb, added updates and security in sources.list, switched to debian-sagemath, added cups)

# alpha4
kabuga (aims 2016-17; Tue Apr 11 try to fix windows booting -- it keeps booting repair).
zaza (uct, firmware-realtek+rtl8723be not working, UPDATE kernel 4.9: working but badly; fixed ant_sel=1)
abdulsalaam (wits, rtl8723be working but badly)
abdulrahman (2016-17, rtl8191 seems to work just fine after installing firmware)

# beta1
melissa (jan, hp, might have given back without sagemath and spyder??)
rendani (yae, added cups to aims-desktop)
danielm (kyle, acer, needed r8169-dkms)
? (jonathan, hp)
shane@aims.ac.za (jonathan, hp laptop, installed no problems)
gekonga (kyle, failed 16.04 upgrade, replace with a-d-d, windows not detected by grub, efi/microsoft folder missing)
joslain (aims 2016-17 Dell Inspiron 15, i5 5200, 4G, R7k; UPDATE 20 Mar - update beta1-beta5, complains about sources.list vs .d/debian-nonfree.list)
bashirat (needed broadcom-sta-dkms for wireless)
mhlasakululeka (preserve /home from ubuntu)
jamie (hp 650)
eyaya (lenovo x250, 180G SSD)

# beta 4
vuyo (kyle, lenovo, needed firmware-atheros for wifi, boot to USB from within Windows 10 settings menu) 
??thabang??mathonsi?? (ideapad)
exavery 
bode (alumni 2009, uct completing phd, old lenovo shuts down to overheating often)
gloria (aims 2016-17, HP)

# beta 5
? (yae)
funmite (grub mode 16 colours?)

# beta 3
Brave Mwanza

# beta5? 6?
asmaa (need to check UEFI/Legacy ... installer not seeing windows)
morris
lesiba (2x laptops)
smegnsh (rtl8188ee wireless not working REVERTED TO UBUNTU 14.04)

# rc2
alicenanyanzi
nolubabalo (stellenbosch, lenovo yoga 300, https://patchwork.kernel.org/patch/9337413/)
rita (aims, inspiron, windows booting already broken, kabuga had similar lapto pwhich windows booting broke AFTER linux installation)
irene (AIMS, SU, SACEMA? Biomath? Inspiron15)
Samar Elsheikh (UCT Bio, often visit AIMS)
bernard (ThinkPad 410?)

# rc3
urgent (Urgent Tsuro, mmed visitor, Toshiba, internal card AR9285 ath9k not working; external usb dongle ar3011 rtl8192cu barely working, needs to use /usr/local/bin/mecer-wifi-up.sh AIMS to connect which runs iwconfig wlx0810770b4ac8 essid $1; dhclient wlx0810770b4ac8; # that takes few seconds to connect and 50% packet loss persists. Neither wireless device can be turned on with rfkill or network manager; managed to install AR2985 driver in Windows and turn on internal wireless; now also works in Linux.).
john (John Macharaga johnmacharaga@gmail.com, HP ProBook 6360b, deleted sda3 Windows recovery and sda4 HP Recovery after asking him; MMED visitor)
evelyn (SM2016-17 Dell latitude 6320)
gift (Gift Tapedzesa, Prolin, MMED visitor)
christian geneus (new orleans, MMED, Lenovo Yoga, i915.modeset=0 to remove flicker, BIOS SATA IntelRST to AHCI to detect disk, cgeneus@tulane.edu)
zinhle (alumna, zinhle@aims.ac.za, acer, convinced her to take a-d instead of mint)
